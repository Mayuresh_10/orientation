# Git Basics
## What is Git?
* Git is version-control software that software developers use to collaborate with different people on a single project.
* Git lets you easily keep track of every revision you and your team make during the development of your software.
* Git helps you keep track of the changes you make to your code, along with the information of the person who changed the code from your team with a timestamp.

### Different Terminologies Used In Git: 
* **Repository:** A repository or "repo" is a directory or storage space for your projects.
* **Commit:** A commit is like saving a file with an unique ID assigned to it so that it can be revised easily.
* **Push:** It means adding your commits to gitlab
* **Branch:** Each repo may have different branches like branches of a tree which eventually gets merged  back into the master branch.
* **Merge:** It means merging the branches in master branch.
* **Cloning:** It means copying down the whole Repository in your locol machine.
* **Fork:** It is similar to Cloning but instead of local machine , the repo will be saved under your name in git.


**Diagram of a simple Git Workflow**
![](https://cdn-media-1.freecodecamp.org/images/1*iL2J8k4ygQlg3xriKGimbQ.png)